## Image bauen

    docker build -t tksuite-es .

## X-Umgebung vorbereiten

### macOS

Unter macOS ist der folgende Zwischenschritt nötig. Es muss zunächst **xhost** gestartet werden, um Verbindungen von der lokalen Maschine anzunehmen:

```
ip=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}')
xhost + $ip
```

## Container starten

### Unter macOS

    docker run -d  --name tksuite-es -e DISPLAY=$ip:0 tksuite-es:latest

### Unter Linux

    docker run -d  --name tksuite-es -e DISPLAY=$DISPLAY tksuite-es:latest